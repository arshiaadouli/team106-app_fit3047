<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\Core\Configure;

use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

/**
 * Payments Controller
 *
 *
 * @method \App\Model\Entity\Payment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $clientId = Configure::read('Site.PAYPAL_clientId');
            $secret = Configure::read('Site.PAYPAL_secret');
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    $clientId,  // you will get information about client id and secret once you have created test account in paypal sandbox
                    $secret
                )
            );

            $price = $this->request->getData('amount');
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_1 = new Item();
            $item_1->setName('Membership Plan')/** item name **/
            ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($price);
            /** unit price **/
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));
            $amount = new \PayPal\Api\Amount();
            $amount->setCurrency('USD')
                ->setTotal($price);
            $transaction = new \PayPal\Api\Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('My Membership');
            $redirect_urls = new \PayPal\Api\RedirectUrls();
            $redirect_urls->setReturnUrl(Router::url(['controller' => 'Payments', 'action' => 'status'], true))/** Specify return URL **/
            ->setCancelUrl(Router::url(['controller' => 'Payments', 'action' => 'status'], true));
            $payment = new \PayPal\Api\Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
            try {
                $payment->create($apiContext);
            } catch (\PayPal\Exception\PPConnectionException $ex) {
                $this->Flash->error('Something went wrong! Please try again.');
                return $this->redirect(['action' => 'index']);

            }
            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect = $link->getHref();
                    break;
                }
            }
            $this->getRequest()->getSession()->write('paypalPaymentId', $payment->getId());

            if (isset($redirect)) {
                return $this->redirect($redirect);
            }
            $this->Flash->error('Something went wrong! Please try again.');
            return $this->redirect(['action' => 'index']);

        }

    }

    public function status()
    {
        $clientId = Configure::read('Site.PAYPAL_clientId');
        $secret = Configure::read('Site.PAYPAL_secret');
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $clientId,
                $secret
            )
        );

        $paymentSessionId = $this->getRequest()->getSession()->read('paypalPaymentId');

        //$this->getRequest()->getSession()->delete('paypalPaymentId');
        $getPayeerId = $this->request->getQuery('PayerID');
        $token = $this->request->getQuery('token');


        if (empty($getPayeerId) || empty($token)) {
            $this->Flash->error('Something went wrong! Please try again.');
            return $this->redirect(['action' => 'index']);
        }
        $payment = Payment::get($paymentSessionId, $apiContext);
        $execution = new \PayPal\Api\PaymentExecution();
        $execution->setPayerId($getPayeerId);

        try {
            $result = $payment->execute($execution, $apiContext);
        } catch (PayPal\Exception\PPConnectionException $ex) {
            //echo '<pre>';print_r(json_decode($ex->getData()));exit;

        }

        // echo '<pre>'; print_r($result); echo '</pre>';

        if ($result->getState() == 'approved') {
            $this->Flash->success('Payment Successfull.');
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error('Payment failed.');
        return $this->redirect(['action' => 'index']);
    }

}