
<?= $this->Form->create(null,['class'=>'cls-form']) ?>
    <h2 class="w3-text-blue">Payment CakePHP Form</h2>

    <label class="w3-text-blue"><b>Enter Amount</b></label>
<?= $this->Form->control('amount',['class'=>'form-control','value'=>100]) ?>
    <button class="btn btn-primary">Pay with PayPal</button></p>

<?= $this->Form->end() ?>