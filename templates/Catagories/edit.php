<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catagory $catagory
 */
?>
<div class="row">
<div class="row d-flex justify-content-center">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $catagory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $catagory->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Catagories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    
    <div class="column-responsive column-80">
        <div class="catagories form content">
            <?= $this->Form->create($catagory) ?>
            <fieldset>
                <legend><?= __('Edit Catagory') ?></legend>
                <?php
                    echo $this->Form->control('name');
                ?>
             <div style="text-align:center; margin-top:20;" class="mt-5">
                        <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
                    </div>
                    <?= $this->Form->end() ?>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</div>
</div>
