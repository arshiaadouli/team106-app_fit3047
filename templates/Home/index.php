<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Colrolib Templates">
  <meta name="author" content="Colrolib">
  <meta name="keywords" content="Colrolib Templates">

  <!-- Title Page-->
  <title>Au Form Wizard</title>

  <!-- Icons font CSS-->
  <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
  <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
  <!-- Font special for pages-->
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Vendor CSS-->
  <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
  <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

  <!-- Main CSS-->
  <link href="css/main.css" rel="stylesheet" media="all">
  <?= $this->Html->css('main') ?>


  <style>
    input,
    select,
    textarea {
      color: #000000;
    }

    textarea:focus,
    input:focus {
      color: #001100;
    }
  </style>
</head>

<!-- Header-->
<!-- <header class="bg-dark py-5">
                <div class="container px-5">
                    <div class="row gx-5 align-items-center justify-content-center">
                        <div class="col-lg-8 col-xl-7 col-xxl-6">
                            <div class="my-5 text-center text-xl-start">
                                <h1 class="display-5 fw-bolder text-white mb-2">The perfect Venue for a wedding</h1>
                                <p class="lead fw-normal text-white-50 mb-4">This venue can cater for DJ's, and make your next wedding party one to rememeber</p>

                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">

                                        <?= $this->Html->link("Learn More!", "details/view/2", ['class' => 'btn btn-primary']) ?>

                                    </div>

                            </div>
                        </div>
                        <div class="col-xl-5 col-xxl-6 d-none d-xl-block text-center"><?= $this->Html->image("venues/pic1.jpg", array('class' => 'card-img-top')) ?></div>
                    </div>
                </div>
            </header> -->
<!-- Features section-->
<!-- <section>
                <header class="bg-dark py-5">
                    <div class="container px-4 px-lg-5 my-5">
                        <div class="text-center text-white">
                            <h1 class="display-4 fw-bolder" style="color:white">Find the Venue for you!</h1>
                            <p class="lead fw-normal text-white-50 mb-0">The best venues on the market for any size event</p>
                        </div>
                    </div>
                </header> -->


<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-end align-items-center">
  <div id="heroCarousel" data-bs-interval="5000" class="container carousel carousel-fade" data-bs-ride="carousel">

    <!-- Slide 1 -->
    <div class="carousel-item active">
      <div class="carousel-container">
        <h2 class="animate__animated animate__fadeInDown">Welcome to <span>Lavender Entertainment</span></h2>
        <p class="animate__animated fanimate__adeInUp">We offer the best venues for any event</p>

      </div>
    </div>

  </div>

  <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
    <defs>
      <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
    </defs>
    <g class="wave1">
      <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
    </g>
    <g class="wave2">
      <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
    </g>
    <g class="wave3">
      <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
    </g>
  </svg>

</section><!-- End Hero -->

<br>
<br>
<!--Search-->
<?php
if(!(isset($_SESSION['role']) && $_SESSION['role'] == "admin")){

?>

<div class="container">
  <div class="section-title" data-aos="zoom-out">
    <h2>Search</h2>
    <p>Search your desired venue</p>
  </div>

  <div class="row d-flex justify-content-center">
    <div class="col-md-6">
      <div style="background-color : #26497c; padding : 30px; border-radius : 40px; color : white ">

        <?= $this->Form->create(null, ['type' => 'get']) ?>
        <?= $this->Form->control('key', ['label' => "Venue's name", 'value' => $this->request->getQuery('key'), 'style' => 'color:#000']) ?>
        <?= $this->Form->control('minimun_capacity', ['label' => 'Minimum capacity', 'value' => $this->request->getQuery('minimun_capacity'), 'type' => 'number', 'style' => 'color:#000']) ?>
        <?= $this->Form->control('maximum_capacity', ['label' => 'Maximum capacity', 'value' => $this->request->getQuery('maximum_capacity'), 'type' => 'number', 'style' => 'color:#000']) ?>
        <?= $this->Form->control('minimum_price', ['label' => 'Minimum price', 'value' => $this->request->getQuery('minimum_price'), 'type' => 'number', 'style' => 'color:#000']) ?>
        <?= $this->Form->control('maximum_price', ['label' => 'Maximum price', 'value' => $this->request->getQuery('maximum_price'), 'type' => 'number', 'style' => 'color:#000']) ?>

        <label for="category">Category</label>
        <select name="category" style="color:#000">
          <option value='0'></option>
          <?php
          foreach ($cats as $c) {            
            if ($c->id == $this->request->getQuery('category')) {
              echo "<option value='" . $c->id .  "' selected='selected'>" . $c->name . "</option>";
            } else {
              echo "<option value='" . $c->id .  "'>" . $c->name . "</option>";
            }
          }
          ?>
        </select>



        <br>
        <div class="text-center" style="padding:50px">
          <?= $this->Form->Button('Submit', ['class' => 'btn btn-success']) ?>
        </div>
        <?= $this->Form->end() ?>
      </div>

    </div>
  </div>
  <!-- Section-->
  <section id="team" class="team">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Venues</h2>
        <p>Our current venues</p>
      </div>
      <div class="row">
        <?php if (count($venues) > 0) { ?>
          <?php foreach ($venues as $venue) { ?>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
              <div class="member" data-aos="fade-up">
                <!-- Product image-->
                <!-- <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." /> -->
                <div class="card-img-top" style="width:200px;height:200px">
                    <?= @$this->Html->image($venue->image, array('width'=>'200px', 'height'=>'200px')) ?>
                </div>


                <!-- Product details-->
                <div class="card-body p-4">
                  <div class="text-center">
                    <!-- Product name-->
                    <h5 class="fw-bolder"><?= $venue->name ?></h5>
                    <!-- Product price-->

                  </div>
                </div>
                <!-- Product actions-->
                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
               <div style="text-align:center; margin-top:20;" class="mt-5">
                  <?= $this->Html->link("Learn More!", "details/view/" . $venue->id, ['class' => 'btn btn-primary']) ?>
                </div>
                </div>
              </div>
            </div>



          <?php }
        } else { ?>
          <h1>No events are found!</h1>
        <?php } ?>

      </div>
    </div>
</div>
</section>


</section>
<?php }?>


<script src="vendor/jquery/jquery.min.js"></script>
<!-- Vendor JS-->
<script src="vendor/select2/select2.min.js"></script>
<script src="vendor/jquery-validate/jquery.validate.min.js"></script>
<script src="vendor/bootstrap-wizard/bootstrap.min.js"></script>
<script src="vendor/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="vendor/datepicker/moment.min.js"></script>
<script src="vendor/datepicker/daterangepicker.js"></script>

<!-- Main JS-->
<!-- <script src="js/global.js"></script> -->
<?= $this->Html->script('global.js') ?>