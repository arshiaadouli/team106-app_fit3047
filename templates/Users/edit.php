<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <div class="row d-flex justify-content-center">
   
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Enquiries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>

           
        </div>
    
   <div class="column-responsive column-80">
        <div class="users form content">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <?= $this->Form->create($user, ['type' => 'file']) ?>
                    <fieldset>
                        <legend>
                            <h2><?= __('Edit User') ?></h2>
                        </legend>
                        <?php
                        echo $this->Form->control('first_name');
                        echo $this->Form->control('last_name');
                        echo $this->Form->control('address');
                        echo $this->Form->control('email');
                        echo $this->Form->control('phone_number');
                        echo $this->Form->control('dob');
                        echo $this->Form->control('password');
                        echo $this->Form->control('image_file', ['type' => 'file']);
                        echo $this->Form->control('role', [
                            'options' => ['admin' => 'Admin', 'user' => 'User']
                        ]) ?>

                    </fieldset>
                    <div style="text-align:center; margin-top:20;" class="mt-5">
                        <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
                    </div>
                    <?= $this->Form->end() ?>

                </div>
                <div class="col-3"></div>
                </div>
            </div>
        </div>
    </div>
</div>
