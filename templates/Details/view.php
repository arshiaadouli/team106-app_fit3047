<!-- <?php
// echo $venue->id;
// echo $venue->name;
// ?> -->

<style>

    form{
      display: inline-block;
        
    }
</style>


<!-- Responsive navbar-->

<!-- Page content-->
<div style="margin-left:7%" class="container" >
<div class="col-xs-1 center-block">
    <div class="row">
        <div>
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <!-- Post title-->
                    <h2 class="fw-bolder mb-1">Venue's Name: <?= $venues->name?></h1>

                </header>
                <!-- Preview image figure-->
                <!-- <figure class="mb-4"><img class="img-fluid rounded" src="https://dummyimage.com/900x400/ced4da/6c757d.jpg" alt="..." /></figure> -->


    <?php
    // $vendors_id = [];
    // $vendors_name = [];
    // foreach ($vendors as $vendor){
    //     array_push($vendors_id, $vendor->id);
    //     array_push($vendors_name, $vendor->name);
    // }

    $vendor = array();

        foreach ($vendors as $x){
            $id = $x->id;
            $name = $x->name;
            $price = $x->price;
            $description = $x->description;
            $start = $x->start_availability;
            $end = $x->end_availability;
            $start_vendor = $start->i18nFormat('dd-MM-yyyy');
            $end_vendor = $end->i18nFormat('dd-MM-yyyy');
            $start_venue = $venues->start_availability->i18nFormat('dd-MM-yyyy');
            $end_venue = $venues->end_availability->i18nFormat('dd-MM-yyyy');

         //   echo $start_vendor . ' ' . $end_vendor . ' ' . $start_venue . ' ' . $end_venue.'';
            if($start_vendor >= $start_venue && $end_vendor <= $end_venue){
                $vendor[$id] = "Vendor's Name : ".$name."- Vendor's cost : ".$price."- Vendor's description : ".$description;
            }
        }

    ?>

        <div class="row">
            <!--<div class="col-8">-->

                <br>
                <br>
                <?= $this->Form->create(null, ['url'=>['action'=>'response/?venue='.$venues->id]]); ?>
                <div class="form-check">
                                
                <div>

                    <?= @$this->Html->image($venues->image, array('class'=>'img-fluid rounded', 'width'=>'500px', 'height'=>'500px')) ?>
                </div>
                <br>
                <br>
                
                <?php
                

                ?>
                <?php if($vendor){?>
                <?php } ?>
                <br>
                <div class="d-flex">
                <div style="background-color : white; border-radius : 40px;  padding:20px">
                <h3>Select vendor(s) to add events!</h3>
                <br>
                <br>
                <?= $this->Form->select('vendor', $vendor,
                
                [
            
                'multiple' => 'checkbox', 'class' =>['form-check-input']
                ]);?>
                </div>
                <br/>
                <br/>



                        <br>
                        <div style="background-color : white; padding : 30px; border-radius : 40px; display:inline-block;padding:20px; margin-left:75px">
                        <legend><?= __('To complete your booking complete the form below') ?></legend>
                        <br>
                        <?= $this->Form->control('name', ['required' => true]) ?>
                        <br>
                        <?= $this->Form->control('email', ['required' => true]) ?>
                        <br>
                        <?= $this->Form->control('address', ['required' => true]) ?>
                        <br>
                        <br>
                        <label>Date of Booking</label>
                        <?= $this->form->date('date') ?>

                        <br>
                        <br>
                        
                        
                    <?= $this->Form->submit('submit');?>
                    </div>
                    </div>
                <?= $this->Form->end(); ?>
                

                    </div>
                </div>
            </div>

            


                <br>

                
            </div>
        </div>


        

                <!-- Post content-->
                <section class="mb-5" style="margin-top:50px">
                    <h2 class="fw-bolder mb-4 mt-5">ِLocation:</h2>
                    <h3 class="fw-bolder mb-4 mt-5">ِStreet:</h2>
                    <p class="fs-5 mb-4"><?= $venues->street ?></p>
                    <hr>
<br>
                    <h3 class="fw-bolder mb-4 mt-5">Suburb:</h2>
                    <p class="fs-5 mb-4"><?= $venues->suburb ?></p>
                    <hr>
<br>
                    <h3 class="fw-bolder mb-4 mt-5">State:</h2>
                    <p class="fs-5 mb-4"><?= $venues->state ?></p>
                    <hr>
<br>
                    <h3 class="fw-bolder mb-4 mt-5">Postcode:</h2>
                    <p class="fs-5 mb-4"><?= $venues->postcode ?></p>
                    <hr>
<br>
                    <h3 class="fw-bolder mb-4 mt-5">Availability:</h2>
                    <p class="fs-5 mb-4"><?= $venues ->start_availability ?> - <?= $venues ->end_availability ?></p>
                    <hr>

                    
                    <h2 class="fw-bolder mb-4 mt-5">ِDescription:</h2>
                    <p class="fs-5 mb-4"><?= $venues->description ?></p>
                    <hr>

                    <h2 class="fw-bolder mb-4 mt-5">Capacity:</h2>
                    <p class="fs-5 mb-4"><b><?= $venues->capacity ?></b> people</p>
                    <hr>

                
            </article>
        </div>
    </div>


    
</div>
</div>

