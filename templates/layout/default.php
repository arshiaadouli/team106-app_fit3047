<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />



        <!-- <link rel="icon" type="image/x-icon" href="assets/favicon.ico" /> -->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <!-- <link href="css/styles.css" rel="stylesheet" /> -->



            <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colrolib Templates">
    <meta name="author" content="Colrolib">
    <meta name="keywords" content="Colrolib Templates">

    <!-- Title Page-->
    <title>Lavender Entertainment</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/styles.css" rel="stylesheet" media="all">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake', 'styles']) ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?php
    if(isset($_SESSION['email'])){
    // echo $_SESSION['id'];
    // echo $_SESSION['image'];
    // echo $_SESSION['email'];
    }

    //    echo $this->loadModel('Users');
    //     $recentArticles = $this->Articles->find()->where(
    //         ['email'=>$_SESSION['email']]
    //     );

    ?>
<body class="d-flex flex-column h-100">
        <main class="flex-shrink-0">
            <!-- Navigation-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container px-5">
                    <a class="navbar-brand" href="/home">Lavender Entertainment</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="z-index: 10000;">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item"><a class="nav-link" href="/home">Home</a></li>
                            
                            
                            
                            <?php

                            if(isset($_SESSION['role'])&& $_SESSION['role']==='user'){
                                ?>
                                    
       
                                    <li class="nav-item"><a class="nav-link" href="/contact">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/feedback">Feedback</a></li>



                                    

                            <?php
                            }?>
                            

                            <?php

                            if(!isset($_SESSION['role'])){
                                ?>
                                    
                                    <li class="nav-item"><a class="nav-link" href="/users/login">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/users/add">Register</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/contact">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/feedback">Feedback</a></li>



                                    

                            <?php
                            } else{
                                if(isset($_SESSION['role']) && $_SESSION['role']==='admin' ){
                            ?>

                            <li class="nav-item"><a class="nav-link" href="/users">Users</a></li>
                            <li class="nav-item"><a class="nav-link" href="/events">Events</a></li>
                            <li class="nav-item"><a class="nav-link" href="/vendors">Vendors</a></li>
                            <li class="nav-item"><a class="nav-link" href="/venues">Venues</a></li>
                            <li class="nav-item"><a class="nav-link" href="/catagories">Categories</a></li>
                            <li class="nav-item"><a class="nav-link" href="/evaluation">Evaluation</a></li>
                            <li class="nav-item"><a class="nav-link" href="/enquiries">Enquiries</a></li>
                

                            <?php } ?>
                            
                            <li class="nav-item"><a class="nav-link" href="/users/logout">Logout</a></li>



                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="avatar" href="/users/profile" role="link" data-bs-toggle="dropdown" aria-expanded="false">
                                <?= @$this->Html->image($_SESSION['image'],['width'=>'20px', 'heigth'=>'20px'] )?>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownPortfolio">
                                    <li>
                                       Email: <?= $_SESSION['email'] ?>
                                    </li>
                                    <li><a class="dropdown-item" href=<?= "/users/view/".$_SESSION['id'] ?>>profile</a></li>

                                </ul>
                                
                                
                            </li>
                            <?php
                            }
                                                            if(isset($_SESSION['price']) && $_SESSION['price']>0 ){
                            
                            ?>
                            <?php if((!($_SESSION['role']=='admin'))){?>
                            <li class="nav-item"><a class="nav-link" href="/stripe/">Checkout</a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- Header-->



        </main>
    <main class="main">

            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>

 <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/jquery-validate/jquery.validate.min.js"></script>
    <script src="vendor/bootstrap-wizard/bootstrap.min.js"></script>
    <script src="vendor/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/scripts.js"></script>



        <!-- Core theme JS-->
        <!-- <script src="js/scripts.js"></script> -->
        <?= $this->Html->script('scripts.js') ?>
        <footer class="bg-dark py-4 mt-auto">
            <div class="container px-5">
                <div class="row align-items-center justify-content-between flex-column flex-sm-row">
                    <div class="col-auto"><div class="small m-0 text-white">Copyright &copy; Lavender Entertainment 2021</div></div>

                </div>
            </div>

    </footer>
</body>
</html>


