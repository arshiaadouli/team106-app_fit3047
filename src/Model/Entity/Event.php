<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property int $venue_id
 * @property int $vendor_id
 * @property string $name
 * @property string $email
 * @property string $address
 * @property \Cake\I18n\FrozenDate $date
 * @property bool $payment
 *
 * @property \App\Model\Entity\Venue $venue
 * @property \App\Model\Entity\Vendor[] $vendors
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'venue_id' => true,
        'vendor_id' => true,
        'name' => true,
        'email' => true,
        'address' => true,
        'date' => true,
        'payment' => true,
        'venue' => true,
        'vendors' => true,
    ];
}
