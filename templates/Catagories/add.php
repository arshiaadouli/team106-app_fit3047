<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catagory $catagory
 */
?>
<div class="row d-flex justify-content-center">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Catagories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="catagories form content">
        <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?= $this->Form->create($catagory) ?>
            <fieldset>
                <legend><?= __('Add Catagory') ?></legend>
                <?php
                    echo $this->Form->control('name');
                ?>
            </fieldset>
            <div style="text-align:center; margin-top:20;" class="mt-5">
            <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
            <?= $this->Form->end() ?>
            </div>
                <div class="col-3"></div>
                </div>
        </div>
    </div>
</div>
