<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vendor $vendor
 * @var \Cake\Collection\CollectionInterface|string[] $events
 */
?>
<div class="row d-flex justify-content-center">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Vendors'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="vendors form content">
            <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?= $this->Form->create($vendor) ?>
            <fieldset>
                <legend><?= __('Add Vendor') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('contact_number');
                    echo $this->Form->control('contact_email');
                    echo $this->Form->control('price');
                    echo $this->Form->control('start_availability');
                    echo $this->Form->control('end_availability');

                ?>
            </fieldset>
            <div style="text-align:center; margin-top:20;" class="mt-5">
            <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
            </div>
            <?= $this->Form->end() ?>
            </div>
                <div class="col-3"></div>
                </div>
        </div>
    </div>
</div>
