<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatagoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatagoriesTable Test Case
 */
class CatagoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatagoriesTable
     */
    protected $Catagories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Catagories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Catagories') ? [] : ['className' => CatagoriesTable::class];
        $this->Catagories = $this->getTableLocator()->get('Catagories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Catagories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CatagoriesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
