<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vendor Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $image
 * @property string $contact_number
 * @property string|null $contact_email
 * @property int $price
 * @property \Cake\I18n\FrozenTime $start_availability
 * @property \Cake\I18n\FrozenTime $end_availability
 *
 * @property \App\Model\Entity\Event[] $events
 */
class Vendor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'image' => true,
        'contact_number' => true,
        'contact_email' => true,
        'price' => true,
        'start_availability' => true,
        'end_availability' => true,
        'events' => true,
    ];
}
