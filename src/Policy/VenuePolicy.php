<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Venue;
use Authorization\IdentityInterface;

/**
 * Venue policy
 */
class VenuePolicy
{
    /**
     * Check if $user can create Venue
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Venue $venue
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Venue $venue)
    {
    }

    /**
     * Check if $user can update Venue
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Venue $venue
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Venue $venue)
    {
    }

    /**
     * Check if $user can delete Venue
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Venue $venue
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Venue $venue)
    {
    }

    /**
     * Check if $user can view Venue
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Venue $venue
     * @return bool
     */
    public function canView(IdentityInterface $user, Venue $venue)
    {
    }
}
