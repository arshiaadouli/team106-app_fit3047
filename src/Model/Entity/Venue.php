<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Venue Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $capacity
 * @property string $image
 * @property \Cake\I18n\FrozenTime $start_availability
 * @property \Cake\I18n\FrozenTime $end_availability
 * @property string $street
 * @property string $suburb
 * @property string $state
 * @property int $postcode
 * @property int $catagories_id
 * @property int $price
 *
 * @property \App\Model\Entity\Catagory $catagory
 * @property \App\Model\Entity\Event[] $events
 */
class Venue extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'capacity' => true,
        'image' => true,
        'start_availability' => true,
        'end_availability' => true,
        'street' => true,
        'suburb' => true,
        'state' => true,
        'postcode' => true,
        'catagories_id' => true,
        'price' => true,
        'catagory' => true,
        'events' => true,
    ];
}
