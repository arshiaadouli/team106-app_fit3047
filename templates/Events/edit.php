<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 * @var string[]|\Cake\Collection\CollectionInterface $venues
 * @var string[]|\Cake\Collection\CollectionInterface $vendors
 */
?>
<div class="row">
<div class="row d-flex justify-content-center">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $event->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $event->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Events'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
        
   <div class="column-responsive column-80">
        <div class="events form content">
        <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
            <?= $this->Form->create($event) ?>
            <fieldset>
                <legend><?= __('Edit Event') ?></legend>
                <?php
                    echo $this->Form->control('venue_id', ['options' => $venues]);
                    echo $this->Form->control('vendor_id');
                    echo $this->Form->control('name');
                    echo $this->Form->control('email');
                    echo $this->Form->control('address');
                    echo $this->Form->control('date');
                    echo $this->Form->control('payment');
                ?>
            </fieldset>
          <div style="text-align:center; margin-top:20;" class="mt-5">
                        <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
                    </div>
                    <?= $this->Form->end() ?>
            </div>
                <div class="col-3"></div>

            </div>
           </div>
        </div>
    </div>
</div>
