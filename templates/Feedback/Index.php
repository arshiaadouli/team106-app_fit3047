<div class="container">
    <br>
    <br>
<div class="feedback form">
    <?= $this->Flash->render() ?>
    <h1>How did we go?</h1>
    <?= $this->Form->create(null, [
    'url' => [
        'controller' => 'Feedback',
        'action' => 'response'
    ]
]);

     ?>
    <fieldset>
                            <div class="row d-flex justify-content-center">
                          <div class="col-md-6">
                               <div style="background-color : white; padding : 30px; border-radius : 40px; border: 1px solid black">
                                    

                                        <br>
                                        <legend><?= __('Please enter feedback here') ?></legend>
                                        <br>
                                        <?= $this->Form->control('name', ['required' => true]) ?>
                                        <br>
                                        <?= $this->Form->control('email', ['required' => true]) ?>
                                        <br>
                                        <?= $this->Form->control('rating', [
                                        'options' => ['1' => '1 (lowest)', '2' => '2', '3' => '3', '4' => '4', '5' => '5 (highest)']])?>
                                        <br>
                                        <?= $this->Form->control('message', ['required' => true]) ?>
                                               <div class="text-center" style="padding:50px">
                                <?=$this->Form->Button('Submit', ['class'=>'btn btn-success'])?>
                                </div>
        </div></div></div>
    </fieldset>

                         
                            
    <?= $this->Form->end() ?>
</div>
</div>


