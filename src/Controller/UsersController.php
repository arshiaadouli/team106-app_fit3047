<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use PharIo\Manifest\Email;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }



    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if(!$user->getErrors){
                $image = $this->request->getData('image_file');
                $name = $image->getClientFilename();
                $targetPath = WWW_ROOT.'img'.DS.$name;
                if($name)
                $image->moveTo($targetPath);
                $user->image = $name;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if(!$user->getErrors){
                $image = $this->request->getData('image_file');
                $name = $image->getClientFilename();
                $targetPath = WWW_ROOT.'img'.DS.$name;
                if($name)
                $image->moveTo($targetPath);
                $user->image = $name;
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



public function login()
{
    $_SESSION['price']=0;
    // $this->request->allowMethod(['get', 'post']);
    if ($this->request->is('post')) {
        $user = $this->Auth->identify();
        if ($user) {
            session_start();
            $email = $this->request->getData('email');
            if($email){
                $users = TableRegistry::getTableLocator()->get('Users');
                $query = $users->find();
                $query->where(['email' => $email]); // Return the same query object

                foreach ($query as $article) {
                    $_SESSION['id'] = $article->id;
                    $_SESSION['email'] = $article->email;
                    $_SESSION['image'] = $article->image;
                    $_SESSION['role'] = $article->role;
                }
            }

            $this->Auth->setUser($user);
            return $this->redirect($this->Auth->redirectUrl());
        }

        
            $this->Flash->error(__('Invalid username or password'));
            

        

    }
   

}



public function logout()
{
    // $result = $this->Authentication->getResult();
    // regardless of POST or GET, redirect if user is logged in
    // if ($result->isValid()) {
        // $this->Authentication->logout();
        unset($_SESSION['email']);
        unset($_SESSION['role']);
        $_SESSION['price']=0;
        // return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        return $this->redirect($this->Auth->logout());
        
    }

public function beforeFilter(\Cake\Event\EventInterface $event)
{
    parent::beforeFilter($event);
    // Configure the login action to not require authentication, preventing
    // the infinite redirect loop issue

    $this->Auth->allow(['login', 'logout', 'add','forgotpassword','resetpassword', 'view', 'edit']) ;
}




}






