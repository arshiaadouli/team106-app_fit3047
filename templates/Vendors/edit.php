<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vendor $vendor
 * @var string[]|\Cake\Collection\CollectionInterface $events
 */
?>
<div class="row d-flex justify-content-center">
<div class="row d-flex justify-content-center">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $vendor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $vendor->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Vendors'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
        
   <div class="column-responsive column-80">
        <div class="vendors form content">
        <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?= $this->Form->create($vendor) ?>
            <fieldset>
                <legend><?= __('Edit Vendor') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('description');
                    //echo $this->Form->control('image');
                    echo $this->Form->control('contact_number');
                    echo $this->Form->control('contact_email');
                    echo $this->Form->control('price');
                    echo $this->Form->control('start_availability');
                    echo $this->Form->control('end_availability');
                  //  echo $this->Form->control('events._ids', ['options' => $events]);
                ?>
            </fieldset>
            <div style="text-align:center; margin-top:20;" class="mt-5">
            <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
            </div>
                        <?= $this->Form->end() ?>
        </div>
                <div class="col-3"></div>
                </div>
    </div>
</div>
</div>
</div>