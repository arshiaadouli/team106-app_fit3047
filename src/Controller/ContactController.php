<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\Locator\LocatorAwareTrait;


/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */


class ContactController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        echo "";
        
    }

    public function response()
    {
        $email = $this->request->getData('email');
        $name = $this->request->getData('name');
        $message = $this->request->getData('message');
        //create a table in phpmyadmin
        //insert in enquiry table

        // $enquiryTable = $this->getTableLocator()->get('enquiry');
        $enquiryTable = $this->getTableLocator()->get('Enquiries');
        $enquiry = $enquiryTable->newEmptyEntity();

        $enquiry->email = $email;
        $enquiry->name = $name;
        $enquiry->message = $message;

        if ($enquiryTable->save($enquiry)) {
        // The $enquiry entity contains the id now
            $id = $enquiry->id;

        }

    }

      public function beforeFilter(\Cake\Event\EventInterface $event)
{
    parent::beforeFilter($event);
    // Configure the login action to not require authentication, preventing
    // the infinite redirect loop issue
    $this->Auth->Allow(['index', 'response']);
    }

}
