<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row d-flex justify-content-center">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
        <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?= $this->Form->create($user, ['type'=>'file']) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                    echo $this->Form->control('first_name');
                    echo $this->Form->control('last_name');
                    echo $this->Form->control('address');
                    echo $this->Form->control('email');
                    echo $this->Form->control('phone_number');
                    echo $this->Form->control('dob');
                    echo $this->Form->control('password');
                    echo $this->Form->control('retype_password', ['type' => 'password']);
                    echo $this->Form->control('image_file', ['type'=>'file']);
                    if(isset($_SESSION['role']) && $_SESSION['role']=="admin"){
                        echo $this->Form->control('role', [
                        'options' => ['admin' => 'Admin', 'user' => 'User']
                        ]) ;
                    }
                    else{
                        
                        echo $this->Form->control('role', [
                        'options' => ['user' => 'User']
                        ]) ;
                    
                }
        ?>
                <br>
                <br>
            </fieldset>
          <div style="text-align:center; margin-top:20;" class="mt-5">
            <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
            </div>
            <?= $this->Form->end() ?>
            </div>
                <div class="col-3"></div>
                </div>
        </div>
    </div>
</div>
