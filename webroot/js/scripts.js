/*!
* Start Bootstrap - Modern Business v5.0.4 (https://startbootstrap.com/template-overviews/modern-business)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

/**
* Template Name: Selecao - v4.3.0
* Template URL: https://bootstrapmade.com/selecao-bootstrap-template/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
(function($) {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos - offset,
      behavior: 'smooth'
    })
  }

  /**
   * Toggle .header-scrolled class to #header when page is scrolled
   */
  let selectHeader = select('#header')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
      } else {
        selectHeader.classList.remove('header-scrolled')
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Mobile nav dropdowns activate
   */
  on('click', '.navbar .dropdown > a', function(e) {
    if (select('#navbar').classList.contains('navbar-mobile')) {
      e.preventDefault()
      this.nextElementSibling.classList.toggle('dropdown-active')
    }
  }, true)

  /**
   * Scrool with ofset on links with a class name .scrollto
   */
  on('click', '.scrollto', function(e) {
    if (select(this.hash)) {
      e.preventDefault()

      let navbar = select('#navbar')
      if (navbar.classList.contains('navbar-mobile')) {
        navbar.classList.remove('navbar-mobile')
        let navbarToggle = select('.mobile-nav-toggle')
        navbarToggle.classList.toggle('bi-list')
        navbarToggle.classList.toggle('bi-x')
      }
      scrollto(this.hash)
    }
  }, true)

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Porfolio isotope and filter
   */
  window.addEventListener('load', () => {
    let portfolioContainer = select('.portfolio-container');
    if (portfolioContainer) {
      let portfolioIsotope = new Isotope(portfolioContainer, {
        itemSelector: '.portfolio-item',
      });

      let portfolioFilters = select('#portfolio-flters li', true);

      on('click', '#portfolio-flters li', function(e) {
        e.preventDefault();
        portfolioFilters.forEach(function(el) {
          el.classList.remove('filter-active');
        });
        this.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: this.getAttribute('data-filter')
        });
        portfolioIsotope.on('arrangeComplete', function() {
          AOS.refresh()
        });
      }, true);
    }

  });

  /**
   * Initiate portfolio lightbox 
   */
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });

  /**
   * Portfolio details slider
   */
  new Swiper('.portfolio-details-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Testimonials slider
   */
  new Swiper('.testimonials-slider', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },

      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });

  /**
   * Animation on scroll
   */
  window.addEventListener('load', () => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
      once: true,
      mirror: false
    })
  });



   
    try {
			$("#input-start").daterangepicker(
				{
					ranges: true,
					autoApply: true,
					applyButtonClasses: false,
					autoUpdateInput: false,
				},
				function (start, end) {
					$("#input-start").val(start.format("MM/DD/YYYY"));
					$("#input-end").val(end.format("MM/DD/YYYY"));
				}
			);

			$("#input-start-2").daterangepicker(
				{
					ranges: true,
					autoApply: true,
					applyButtonClasses: false,
					autoUpdateInput: false,
				},
				function (start, end) {
					$("#input-start-2").val(start.format("MM/DD/YYYY"));
					$("#input-end-2").val(end.format("MM/DD/YYYY"));
				}
			);
		} catch (er) {
			console.log(er);
		}
		/*==================================================================
        [ Single Datepicker ]*/

		try {
			var singleDate = $(".js-single-datepicker");

			singleDate.each(function () {
				var that = $(this);
				var dropdownParent = "#dropdown-datepicker" + that.data("drop");

				that.daterangepicker({
					singleDatePicker: true,
					showDropdowns: true,
					autoUpdateInput: true,
					parentEl: dropdownParent,
					opens: "left",
					locale: {
						format: "DD/MM/YYYY",
					},
				});
			});
		} catch (er) {
			console.log(er);
		}
		/*==================================================================
        [ Special Select ]*/

		try {
			var body = $("body,html");

			var selectSpecial = $("#js-select-special");
			var info = selectSpecial.find("#info");
			var dropdownSelect = selectSpecial.parent().find(".dropdown-select");
			var listRoom = dropdownSelect.find(".list-room");
			var btnAddRoom = $("#btn-add-room");
			var totalRoom = 1;

			selectSpecial.on("click", function (e) {
				e.stopPropagation();
				$(this).toggleClass("open");
				dropdownSelect.toggleClass("show");
			});

			dropdownSelect.on("click", function (e) {
				e.stopPropagation();
			});

			body.on("click", function () {
				selectSpecial.removeClass("open");
				dropdownSelect.removeClass("show");
			});

			listRoom.on("click", ".plus", function () {
				var that = $(this);
				var qtyContainer = that.parent();
				var qtyInput = qtyContainer.find("input[type=number]");
				var oldValue = parseInt(qtyInput.val());
				var newVal = oldValue + 1;
				qtyInput.val(newVal);

				updateRoom();
			});

			listRoom.on("click", ".minus", function () {
				var that = $(this);
				var qtyContainer = that.parent();
				var qtyInput = qtyContainer.find("input[type=number]");
				var min = qtyInput.attr("min");

				var oldValue = parseInt(qtyInput.val());
				if (oldValue <= min) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue - 1;
				}
				qtyInput.val(newVal);

				updateRoom();
			});

			listRoom.on("change", ".inputQty", function () {
				var that = $(this);
				if (isNumber(that.val())) {
					var qtyVal = parseInt(that.val());
					if (that.val().length === 0) {
						qtyVal = 0;
					}

					if (qtyVal < 0) {
						qtyVal = 0;
					}
					that.val(qtyVal);

					updateRoom();
				}
			});

			function isNumber(n) {
				return typeof n != "boolean" && !isNaN(n);
			}

			btnAddRoom.on("click", function (e) {
				e.preventDefault();

				totalRoom++;

				listRoom.append(
					'<li class="list-room__item">' +
						'                                        <span class="list-room__name"> Room ' +
						totalRoom +
						"</span>" +
						'                                        <ul class="list-person">' +
						'                                            <li class="list-person__item">' +
						'                                                <span class="name">' +
						"                                                    Adults" +
						"                                                </span>" +
						'                                                <div class="quantity quantity1">' +
						'                                                    <span class="minus">' +
						"                                                        -" +
						"                                                    </span>" +
						'                                                    <input type="number" min="0" value="0" class="inputQty">' +
						'                                                    <span class="plus">' +
						"                                                        +" +
						"                                                    </span>" +
						"                                                </div>" +
						"                                            </li>" +
						'                                            <li class="list-person__item">' +
						'                                                <span class="name">' +
						"                                                    Children" +
						"                                                </span>" +
						'                                                <div class="quantity quantity2">' +
						'                                                    <span class="minus">' +
						"                                                        -" +
						"                                                    </span>" +
						'                                                    <input type="number" min="0" value="0" class="inputQty">' +
						'                                                    <span class="plus">' +
						"                                                        +" +
						"                                                    </span>" +
						"                                                </div>" +
						"                                            </li>" +
						"                                        </ul>"
				);

				updateRoom();
			});

			function countAdult() {
				var listRoomItem = listRoom.find(".list-room__item");
				var totalAdults = 0;

				listRoomItem.each(function () {
					var that = $(this);
					var numberAdults = parseInt(that.find(".quantity1 > input").val());

					totalAdults = totalAdults + numberAdults;
				});

				return totalAdults;
			}

			function countChildren() {
				var listRoomItem = listRoom.find(".list-room__item");
				var totalChildren = 0;

				listRoomItem.each(function () {
					var that = $(this);
					var numberChildren = parseInt(that.find(".quantity2 > input").val());

					totalChildren = totalChildren + numberChildren;
				});

				return totalChildren;
			}

			function updateRoom() {
				var totalAd = parseInt(countAdult());
				var totalChi = parseInt(countChildren());
				var adults = "Adult, ";
				var rooms = "Room";

				if (totalAd > 1) {
					adults = "Adults, ";
				}

				if (totalRoom > 1) {
					rooms = "Rooms";
				}

				var infoText =
					totalAd +
					" " +
					adults +
					totalChi +
					" " +
					"Children, " +
					totalRoom +
					" " +
					rooms;

				info.val(infoText);
			}
		} catch (e) {
			console.log(e);
		}
		/*[ Select 2 Config ]
        ===========================================================*/

		try {
			var selectSimple = $(".js-select-simple");

			selectSimple.each(function () {
				var that = $(this);
				var selectBox = that.find("select");
				var selectDropdown = that.find(".select-dropdown");
				selectBox.select2({
					dropdownParent: selectDropdown,
				});
			});
		} catch (err) {
			console.log(err);
		}
    

})(jQuery)