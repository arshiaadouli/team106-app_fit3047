<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TempController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

        $articles = TableRegistry::getTableLocator()->get('Users');
        $query = $articles->find();
        $query->where(['email' => "arshiaadouli@gmail.com"]); // Return the same query object
        foreach ($query as $article) {
        echo $article->id;
}
    }
}