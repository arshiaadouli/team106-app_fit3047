<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Venue $venue
 * @var \Cake\Collection\CollectionInterface|string[] $catagories
 */
?>
<div class="row d-flex justify-content-center">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Venues'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="venues form content">
        <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?= $this->Form->create($venue, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Venue') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('capacity');
                    echo $this->Form->control('image', ['type' => 'file']);
                    echo $this->Form->control('start_availability');
                    echo $this->Form->control('end_availability');
                    echo $this->Form->control('street');
                    echo $this->Form->control('suburb');
                    echo $this->Form->control('state');
                    echo $this->Form->control('postcode');
                    echo $this->Form->control('catagories_id', ['options' => $catagories]);
                    echo $this->Form->control('price');
                ?>
            </fieldset>
            <div style="text-align:center; margin-top:20;" class="mt-5">
            <?= $this->Form->submit(__('Submit'), ['style' => 'width:200px; ']) ?>
            <?= $this->Form->end() ?>
            </div>
                <div class="col-3"></div>
                </div>
        </div>
    </div>
</div>
