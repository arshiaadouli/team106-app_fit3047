<?php
$data = [];
foreach ($venues as $venue) {
    $item = [
        'title' => $venue->name,
        'start' => $venue->start_availability,
        'end' => $venue->end_availability
    ];
    $data[] = $item;
}
echo json_encode($data);