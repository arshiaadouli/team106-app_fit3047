<?php

namespace App\Controller;
use Stripe;

class StripesController extends AppController
{
    public function stripe()
    {
        $this->set("title", "Stripe Payment Gateway Integration");
    }

    public function payment()
    {
        require_once VENDOR_PATH. '/stripe/stripe-php/init.php';

        Stripe\Stripe::setApiKey(STRIPE_SECRET);
        $stripe = Stripe\Charge::create ([
            "amount" => $_SESSION['price'] * 100,
            "currency" => "aud",
            "source" => $_REQUEST["stripeToken"],
            "description" => "Test payment via Stripe From onlinewebtutorblog.com"
        ]);
        // after successfull payment, you can store payment related information into your database
        $this->Flash->success(__('Payment done successfully'));

        return $this->redirect(['action' => 'stripe']);
    }
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Auth->Allow(['stripe']);
    }


}   
