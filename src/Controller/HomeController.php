<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Datasource\ConnectionManager;

/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        session_start();
        $result = $this->loadModel('Venues');
        $data = $result->find();
        $this->set('venues', $data);

        // get form data from request
        $key = $this->request->getQuery('key'); // Venue's name
        $minimun_capacity = (int)$this->request->getQuery('minimun_capacity');
        $maximum_capacity = (int)$this->request->getQuery('maximum_capacity');
        $minimum_price = (int)$this->request->getQuery('minimum_price');
        $maximum_price = (int)$this->request->getQuery('maximum_price');
        $category_id = (int)$this->request->getQuery('category');
        //  $description = $this->request->getQuery('description');
        
        $query = $result->find('all');
        if ($key) {
            $query = $query->where(['name like' =>'%'.$key.'%']);
        }
        if ($minimun_capacity) {
            $query =  $query->andWhere([ 'capacity >' => $minimun_capacity]);
        }
        if ($maximum_capacity) {
            $query = $query->andWhere([ 'capacity <' => $maximum_capacity]);
        }
        if ($minimum_price) {
            $query = $query->andWhere([ 'price >' => $minimum_price]);
        }
        if ($maximum_price) {
            $query = $query->andWhere([ 'price <' => $maximum_price]);
        }
        if ($category_id) {
            $query = $query->andWhere([ 'catagories_id' => $category_id]);
        }

        if (!$key && !$minimun_capacity && !$maximum_capacity && !$minimum_price && !$maximum_price && $category_id == 0) {
            $query = $result;
        }

        $venues = $this->paginate($query);
        $this->set(compact('venues'));

        $cat = $this->loadModel('Catagories');
        $cats = $cat->find()->select(['name', 'id']);
        $this->set('cats', $cats);
    }


    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Auth->allow(['index']);
    }


    public function isAuthorized($user)
    {
        // Admin can access every action
        return true;
    }
}
