-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 17, 2021 at 04:57 AM
-- Server version: 5.7.35
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u21s2106_dev`
--
CREATE DATABASE IF NOT EXISTS `u21s2106_dev` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `u21s2106_dev`;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `retype_password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `message` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `message`) VALUES
(1, 'sad', 'asdasd@asddasd', 'asd'),
(2, 'Arshia Adouli', 'qweefwqefarshiaadouli@gmail.com', 'asd'),
(5, 'Darren Something', 'darrensomething@gmial.com', 'Hi I want a big venue.'),
(6, '135', 'brokenemail@gmail.com', 'Hello');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `payment` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `venue_id`, `vendor_id`, `user_id`, `date`, `payment`) VALUES
(5, 1, 3, 13, '0000-00-00', 0),
(6, 1, 4, 13, '0000-00-00', 0),
(7, 1, 2, 12, NULL, NULL),
(8, 1, 3, 12, NULL, NULL),
(9, 3, 3, 12, NULL, NULL),
(10, 3, 4, 12, NULL, NULL),
(11, 3, 3, 13, NULL, NULL),
(12, 3, 4, 13, NULL, NULL),
(13, 1, 1, 13, '2021-09-16', 1),
(14, 1, 2, 13, NULL, NULL),
(15, 4, 1, 13, NULL, NULL),
(16, 2, 1, 13, NULL, NULL),
(17, 2, 2, 13, NULL, NULL),
(18, 2, 3, 13, NULL, NULL),
(19, 2, 4, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `phone_number` varchar(256) NOT NULL,
  `dob` date NOT NULL,
  `password` varchar(256) NOT NULL,
  `image` varchar(100) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `address`, `email`, `phone_number`, `dob`, `password`, `image`, `role`) VALUES
(12, 'Arshia', 'Adouli', '103/1457  North road', 'arshia221211@gmail.com', '+61431143206', '2021-09-09', '$2y$10$ERLqMna0hAAydqhnIXurSu3SGwQmLac8vqVKRTjGhLTpztn8nMOV.', '', 'admin'),
(13, 'Arshia', 'Adouli', '103/1457  North road', 'arshiaadouli@gmail.com', '+61431143206', '2021-09-22', '$2y$10$ieApZUnxOJn7kZjlnkMp8OH97..M3m7lkHlJo5xXcEJIQ51oRcvXy', 'WeChat Image_20210826160327.jpg', 'author');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `image` varchar(256) NOT NULL,
  `availability` date NOT NULL,
  `contact_number` varchar(256) NOT NULL,
  `contact_email` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `description`, `image`, `availability`, `contact_number`, `contact_email`) VALUES
(1, 'DJ Glav', 'desc1', 'image1.jpg', '2021-08-13', '231232132', 'arshiaadouli@gmail.com'),
(2, 'James\' Flowers', 'desc1', 'image1.jpg', '2021-08-13', '231232132', 'arshiaadouli@gmail.com'),
(3, 'Tom\'s Catering', 'desc1', 'image1.jpg', '2021-08-13', '231232132', 'arshiaadouli@gmail.com'),
(4, 'DJ John', 'desc1', 'image1.jpg', '2021-08-13', '231232132', 'arshiaadouli@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `capacity` int(11) NOT NULL,
  `image` varchar(256) NOT NULL,
  `availability` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `description`, `capacity`, `image`, `availability`) VALUES
(1, 'Station Street Capital', 'flower DJ food', 121212, 'pic.jpg', '2021-08-11'),
(2, 'Little Vineyard', 'dancer party', 2323213, 'pic1.jpg', '2021-08-10'),
(3, 'Skyview Tower', 'desc3', 121212, 'pic2.jpg', '2021-08-09'),
(4, 'Orange Winery', 'desc4', 12121, 'pic3.jpg', '2021-08-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_venue` (`venue_id`),
  ADD KEY `event_vendor` (`vendor_id`),
  ADD KEY `event_user` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `event_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `event_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`),
  ADD CONSTRAINT `event_venue` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`);
--
-- Database: `u21s2106_iter2`
--
CREATE DATABASE IF NOT EXISTS `u21s2106_iter2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `u21s2106_iter2`;

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `name`) VALUES
(1, 'Birthday Party'),
(2, 'Dance'),
(3, 'Wedding'),
(4, 'Sports Party');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `retype_password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `message` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `message`) VALUES
(1, 'sad', 'asdasd@asddasd', 'asd'),
(2, 'test', 'test@test.com', 'test'),
(3, 'arshia', 'arshiaadouli@gmail.com', 'hello'),
(4, 'arshiaadouli', 'arshiaadouli@gmail.com', '1234'),
(5, 'SIYANG ZHANG', '1533391488@qq.com', 'Hello world!');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `message` varchar(256) NOT NULL,
  `rating` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluation`
--

INSERT INTO `evaluation` (`id`, `name`, `email`, `message`, `rating`) VALUES
(1, 'arshiaadouli', 'arshiaadouli@gmail.com', '121212', 1),
(2, 'arshiaadouli', 'arshiaadouli@gmail.com', '1212', 1),
(3, 'arshiaadouli', 'arshiaadouli@gmail.com', '121212', 1),
(4, 'arshiaadouli', 'arshiaadouli@gmail.com', 'qwewqeqwe', 4),
(5, 'SIYANG ZHANG', '1533391488@qq.com', 'Hello world!', 5);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `payment` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `venue_id`, `vendor_id`, `name`, `email`, `address`, `date`, `payment`) VALUES
(61, 9, 14, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(62, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(63, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(64, 9, 14, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(65, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(66, 9, 14, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(67, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(68, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(69, 9, 11, 'arshia Adouli', 'arshiaadouli@gmail.com', '121-1 village Mews, caulfield north vic 3162', '2021-10-26', 1),
(70, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(71, 9, NULL, 'arsh', 'arshiaadouli@gmail.com', '121-1 village Mews, caulfield north vic 3162', '2021-10-13', 0),
(72, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(73, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(74, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(75, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(76, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(77, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(78, 9, 14, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-13', 0),
(79, 13, 11, 'SIYANG ZHANG', '1533391488@qq.com', '2/27 SARTON ROAD, CLAYTON VIC 3168', '2021-10-02', 0),
(80, 9, 14, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(81, 9, 16, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(82, 9, 16, 'asd', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(83, 9, 17, 'arshiaaaaaa', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(84, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 1),
(85, 9, NULL, 'arshiaghgh', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 1),
(86, 9, 15, 'arshiallll', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(87, 13, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 1),
(88, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(89, 9, 15, 'SIYANG ZHANG', 'ezio@gmail.com', '2/27 SARTON ROAD, CLAYTON VIC 3168', '2021-10-14', 0),
(90, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(91, 9, 16, 'Arshia Adouli', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(92, 9, NULL, 'Arshia Adouli', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 1),
(93, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(94, 9, 11, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 0),
(95, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(96, 9, 11, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 0),
(97, 9, NULL, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2021-10-14', 1),
(98, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(99, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(100, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(101, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(102, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(103, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(104, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(105, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(106, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(107, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(108, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(109, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(110, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(111, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(112, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(113, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(114, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(115, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(116, 13, NULL, 'Anthony', 'test@test.com', '45 test', '2021-10-14', 1),
(117, 9, NULL, 'Anthony', 'test@test.com', '12 Test Road', '2021-10-14', 1),
(118, 9, 15, 'arshia', 'arshiaadouli@gmail.com', '103/1457  North road', '2222-12-12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `rating` varchar(256) NOT NULL,
  `message` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `phone_number` varchar(256) NOT NULL,
  `dob` date NOT NULL,
  `password` varchar(256) NOT NULL,
  `image` varchar(100) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `address`, `email`, `phone_number`, `dob`, `password`, `image`, `role`) VALUES
(12, 'lisa', 'lisa', 'Sarton road', 'admin@gmail.com', '0431143206', '1998-07-23', '$2y$10$rQKFJVLFMzpxMXiaD20nOum2cVpmWq.ZrAvMmMJFF4k471NcB3NQK', '1287681 (1).jpg', 'admin'),
(19, 'corporate ', 'user', '103/1457  North road', 'user@gmail.com', '+61431143206', '1997-10-16', '$2y$10$EzeryIZLfZDVPfCe0XIB6.k2zaXdYtSWgL379QxWm.piFIqyH60bG', 'pexels-photo-373912.jpeg', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `contact_number` varchar(256) NOT NULL,
  `contact_email` varchar(256) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `start_availability` datetime NOT NULL,
  `end_availability` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `description`, `image`, `contact_number`, `contact_email`, `price`, `start_availability`, `end_availability`) VALUES
(11, 'Arshia Adouli', 'sdf', 'image1.jpg', '+61431143206', 'arshiaadouli@gmail.com', 12, '2021-10-07 15:55:32', '2021-10-28 15:55:34'),
(14, 'arshiaadouli', 'asdsadasd', 'asd', '0431143206', 'arshiaadouli@gmail.com', 121, '2021-10-07 11:11:11', '2021-10-28 11:11:11'),
(15, 'SIYANG ZHANG', 'Singer', 'Image 1', '13896181123', '1533391488@qq.com', 100, '2021-10-11 10:30:00', '2021-10-17 23:58:59'),
(16, 'SIYANG ZHANG', 'kkkjhvjhvjhvjvjvjhvjvjvjvjvjvj', 'Image 1', '13896181123', '1533391488@qq.com', 120, '2021-10-06 10:19:32', '2021-10-16 10:19:35'),
(17, 'SIYANG ZHANG', 'Singer', '', '13896181123', '1533391488@qq.com', 12, '2021-10-06 10:21:24', '2021-10-12 10:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `vendors_events`
--

CREATE TABLE `vendors_events` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendors_events`
--

INSERT INTO `vendors_events` (`id`, `event_id`, `vendor_id`) VALUES
(6, 69, 11),
(7, 69, 14),
(8, 79, 11),
(9, 61, 15);

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `capacity` int(11) NOT NULL,
  `image` varchar(256) NOT NULL,
  `start_availability` datetime NOT NULL,
  `end_availability` datetime NOT NULL,
  `street` varchar(256) NOT NULL,
  `suburb` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `postcode` int(11) NOT NULL,
  `catagories_id` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `description`, `capacity`, `image`, `start_availability`, `end_availability`, `street`, `suburb`, `state`, `postcode`, `catagories_id`, `price`) VALUES
(9, 'Big Room ', 'desc1', 15000, 'pic3.jpg', '2021-10-06 11:11:11', '2021-10-29 11:11:11', '103/1457  North road', 'Clayton', 'VIC', 3168, 1, 1212),
(13, 'Birthday', 'sad', 12, 'pexels-photo-373912.jpeg', '2021-10-13 13:48:58', '2021-11-07 13:49:00', '103/1457  North road', 'Clayton', 'VIC', 3168, 1, 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_venue` (`venue_id`),
  ADD KEY `event_vendor` (`vendor_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_events`
--
ALTER TABLE `vendors_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `con1` (`event_id`),
  ADD KEY `con2` (`vendor_id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `venues_catagories` (`catagories_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `vendors_events`
--
ALTER TABLE `vendors_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `event_venue` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`);

--
-- Constraints for table `vendors_events`
--
ALTER TABLE `vendors_events`
  ADD CONSTRAINT `con1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `con2` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`);

--
-- Constraints for table `venues`
--
ALTER TABLE `venues`
  ADD CONSTRAINT `venues_catagories` FOREIGN KEY (`catagories_id`) REFERENCES `catagories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
