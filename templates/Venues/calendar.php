<?= $this->Html->css('//cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css', ['block' => true]);?>
<?= $this->Html->script('//cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js',  ['block' => true])?>
<style>
  #calendar{
    width: 80%;
    height: 80%;
    margin: 0 auto;
    border: 1px solid;
    padding: 10px;
    box-shadow: 5px 10px 8px 10px #888888;
  }
</style>
<div class="venues index content">
    <h3><?= __('Calendar')?></h3>
    
</div>

<script>


        document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          initialView: 'dayGridMonth',
        
          events: '<?php 
          $this->Url->build([
              'controller' => 'Venues',
              'action' => 'calendar',
              '_ext' => 'json'
        ]);?>'
        });
        calendar.render();
      });


</script>

  <body>
    <div id='calendar'></div>
  </body>