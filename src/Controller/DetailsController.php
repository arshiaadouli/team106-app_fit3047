<?php
declare(strict_types=1);

namespace App\Controller;
   use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DetailsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function view($id=null)
    {
        $venues = $this->loadModel('Venues');
        $vendors = $this->loadModel('Vendors');
        $venues = $venues->get($id);
        $vendors = $vendors->find();
        $this->set('venues', $venues);
        $this->set('vendors', $vendors);
        
    }

    public function response(){
        $vendors = $this->request->getData('vendor'); //vendors id
        $name = $this->request->getData('name'); 
        $email = $this->request->getData('email'); 
        $address = $this->request->getData('address'); 
         $date = $this->request->getData('date');

        if(isset($_SESSION['id']))
        $user_id = $_SESSION['id'];
        $venue = $this->request->getQuery('venue'); // venue id
        $price = $_SESSION['price'];
        $venues = $this->loadModel('Venues');
        $price += $venues->get($venue)->price;

        $eventsTable = $this->getTableLocator()->get('Events');
        $event = $eventsTable->newEmptyEntity();

        if($vendors){

            foreach($vendors as $v){
                $vendor = $this->loadModel('Vendors');
                $price += $vendor->get($v)->price;
                $event->vendor_id = $v;
                $event->venue_id = $venue;
                $event->date = $date;
                $event->name = $name;
                $event->email = $email;
                $event->address = $address;
                $event->payment = 0;
                if ($eventsTable->save($event)) {
                    // The $article entity contains the id now
                    $id = $event->id;
                    $this->set('result', "event table has been updated");
                }

            }

        }else{
                $event->venue_id = $venue;
                $event->date = date("Y/m/d");
                $event->name = $name;
                $event->email = $email;
                $event->payment = 1;
                $event->address = $address;
                if ($eventsTable->save($event)) {
                    // The $article entity contains the id now
                    $id = $event->id;
                    $this->set('result', "event table has been updated");
                }
        }
        $this->set('v', $vendors);
        $_SESSION['price'] = $price;
            // return $this->redirect(
            // ['controller' => 'events', 'action' => 'add']
            // );
         
            
        }
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Auth->Allow(['view', 'response']);
    }

}


    

        //     if($vendors){
        //         foreach($vendors as $v){
        //             $event = $eventsTable->newEmptyEntity();
        //             $event->vendor_id = $v;
        //             $event->venue_id = $venue;
        //             $event->date = date("Y/m/d");
                    
        //         if ($eventsTable->save($event)) {
        //             // The $article entity contains the id now
        //             $id = $event->id;
        //             $this->set('result', "event table has been updated");
        //         }
    
        //     }
    
        // }