<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Catagories Controller
 *
 * @property \App\Model\Table\CatagoriesTable $Catagories
 * @method \App\Model\Entity\Catagory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CalendarController extends AppController
{

  

    public function index()
    {
    
    }

    public function calendar()
    {
       $start_range = $this->request->getQuery('start');
       $end_range = $this->request->getQuery('end');
        $venues = $this->Venues->find();

        if($start_range) {
            $venues->where(['start >= '=> $start_range ]);
        }

        if($end_range) {
            $venues->where(['end <= '=> $end_range ]);
        }

        debug($venues);
        exit;

        $this->set(compact('venues'));
    }
}
  
    