<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\Locator\LocatorAwareTrait;


/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */


class FeedbackController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        
    }

    public function response()
    {
        $email = $this->request->getData('email');
        $name = $this->request->getData('name');
        $rating = $this->request->getData('rating');
        $message = $this->request->getData('message');
        //create a table in phpmyadmin
        //insert in enquiry table


        $FeedbackTable = $this->getTableLocator()->get('Evaluation');
        $feedback = $FeedbackTable->newEmptyEntity();

        $feedback->email = $email;
        $feedback->name = $name;
        $feedback->rating = $rating;
        $feedback->message = $message;

        if ($FeedbackTable->save($feedback)) {
        // The $enquiry entity contains the id now
            $id = $feedback->id;

        }

    }

      public function beforeFilter(\Cake\Event\EventInterface $event)
{
    parent::beforeFilter($event);
    // Configure the login action to not require authentication, preventing
    // the infinite redirect loop issue
    $this->Auth->Allow(['index', 'response']);
    }

}
