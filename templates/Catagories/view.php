<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catagory $catagory
 */
?>
<div class="row">
<div class="row d-flex justify-content-center">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Catagory'), ['action' => 'edit', $catagory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Catagory'), ['action' => 'delete', $catagory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $catagory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Catagories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Catagory'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    
    <div class="column-responsive column-80">
        <div class="catagories view content">
            <h3><?= h($catagory->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($catagory->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($catagory->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>